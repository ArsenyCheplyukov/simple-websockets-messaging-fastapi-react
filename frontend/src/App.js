import React, { useState, useEffect, useRef } from "react";
import "./App.css";

function App() {
  const [clientId] = useState(Math.floor(new Date().getTime() / 1000));
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);

  const webSocketRef = useRef(null);

  useEffect(() => {
    const url = "ws://localhost:8000/ws/" + clientId;
    const ws = new WebSocket(url);
    webSocketRef.current = ws;

    ws.onopen = () => {
      console.log("WebSocket connected");
    };

    ws.onmessage = (e) => {
      const receivedMessage = JSON.parse(e.data);
      setMessages((prevMessages) => [...prevMessages, receivedMessage]);
    };

    // Clean up the WebSocket when the component is unmounted or the URL changes
    return () => {
      ws.close();
      webSocketRef.current = null;
    };
  }, [clientId]);

  const sendMessage = () => {
    const ws = webSocketRef.current;
    if (ws && ws.readyState === WebSocket.OPEN) {
      ws.send(JSON.stringify({ message, clientId }));
      setMessage("");
    } else {
      console.log("WebSocket not open.");
    }
  };

  return (
    <div className="container">
      <h1>Chat</h1>
      <h2>Your client id: {clientId}</h2>
      <div className="chat-container">
        <div className="chat">
          {messages.map((value, index) => (
            <div
              key={index}
              className={
                value.clientId === clientId
                  ? "my-message-container"
                  : "another-message-container"
              }
            >
              <div className={value.clientId === clientId ? "my-message" : "another-message"}>
                <p className="client">Client id: {value.clientId}</p>
                <p className="message">{JSON.parse(value.message).message}</p>
              </div>
            </div>
          ))}
        </div>
        <div className="input-chat-container">
          <input
            className="input-chat"
            type="text"
            placeholder="Chat message ..."
            onChange={(e) => setMessage(e.target.value)}
            value={message}
          />
          <button className="submit-chat" onClick={sendMessage}>
            Send
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
